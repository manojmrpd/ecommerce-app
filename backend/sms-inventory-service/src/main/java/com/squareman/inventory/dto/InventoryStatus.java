package com.squareman.inventory.dto;

public enum InventoryStatus {
	IN_STOCK,
	OUT_OF_STOCK,
	LOW_AVAILABILITY
}
