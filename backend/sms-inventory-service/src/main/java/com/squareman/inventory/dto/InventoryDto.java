package com.squareman.inventory.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class InventoryDto implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer inventoryId;
	private Integer skuId;
	private String skuName;
	private Integer availableQuantity;
	private boolean active;
	private InventoryStatus status;
	private String createdBy;
	private String lastUpdatedBy;

}
