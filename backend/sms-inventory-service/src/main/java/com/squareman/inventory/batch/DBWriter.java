package com.squareman.inventory.batch;

import java.util.List;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.squareman.inventory.entity.Inventory;
import com.squareman.inventory.repository.InventoryRepository;

@Component
public class DBWriter implements ItemWriter<Inventory> {

	@Autowired
    private InventoryRepository inventoryRepository;

    @Override
    public void write(List<? extends Inventory> inventory) throws Exception{
        System.out.println("Data Saved for Users: " + inventory);
        inventoryRepository.saveAll(inventory);
    }

}
