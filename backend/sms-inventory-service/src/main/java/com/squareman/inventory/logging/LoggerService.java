package com.squareman.inventory.logging;


import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Service;
import org.springframework.util.StreamUtils;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;

import com.squareman.inventory.constants.Constants;
import com.squareman.inventory.constants.StringUtil;
import com.squareman.inventory.exceptions.BusinessException;
import com.squareman.inventory.exceptions.CommonErrorCodes;
import com.squareman.inventory.exceptions.SystemException;
@Service
public class LoggerService implements LoggerServiceIF {

	private static final Logger LOGGER = LoggerFactory.getLogger(LoggerService.class);

	private static final Map<String, String> LOGTEMPLATES = new HashMap<>();

	@Value("${${spring.application.name}.profile:PFL}")
	private String appId;

	@Value("${spring.application.name:''}")
	private String component;

	@Value("#{'${logging.request.headers:correlationId,clientId,requestTime,errorCode,retryCount}'.split(',')}")
	private List<String> reqHeaders;

	@Value("#{'${logging.response.headers:correlationId,clientId,source,serviceName,responseTimestamp,errorCode,retryCount}'.split(',')}")
	private List<String> resHeaders;

	@Value("#{'${logging.event.headers:correlationId,clientId,errorCode,retryCount}'.split(',')}")
	private List<String> eventHeaders;

	@Value("#{'${logging.skip.httprequests:}'.split(',')}")
	private List<String> skipHttpRequests;

	@Value("#{'${logging.skip.httpresponses:}'.split(',')}")
	private List<String> skipHttpResponses;

	@Value("#{'${logging.skip.events:}'.split(',')}")
	private List<String> skipEvents;

	@Value("#{'${logging.skip.subscribedevents:}'.split(',')}")
	private List<String> skipSubscribedEvents;

	@Value("${logging.request.enable:true}")
	private String logRequestFlag;

	@Value("${logging.response.enable:true}")
	private String logResponseFlag;

	@Value("${logging.event.enable:true}")
	private String logEventFlag;

	@Value("${logging.error.enable:true}")
	private String logErrorFlag;

	@Value("${logging.debug.enable:true}")
	private String logDebugFlag;

	@Value("${logging.info.enable:true}")
	private String logInfoFlag;

	@Value("${logging.warn.enable:true}")
	private String logWarnFlag;

	private static final List<String> SKIPLIST = new ArrayList<>();

	public void logRequest(HttpServletRequest request, String message) {
		try {
			if (logRequestFlag.equalsIgnoreCase(Constants.TRUE) && !skipHttpRequest(request)) {
				LOGGER.info(StringUtils.replaceEach(readLogTemplate(LoggerConstants.REQUEST),
						new String[] { LoggerConstants.APPID_DATA, LoggerConstants.COMPONENT_DATA, LoggerConstants.OPERATION_DATA, LoggerConstants.URL_DATA, LoggerConstants.HTTPMETHOD_DATA, LoggerConstants.CONTENTTYPE_DATA, LoggerConstants.REQUESTQUERYPARAMS_DATA, LoggerConstants.REQUESTHEADERS_JSON, LoggerConstants.REQUESTBODY_JSON },
						new String[] { appId, component, request.getRequestURI(), request.getRequestURL().toString(), request.getMethod(), request.getContentType(),
								getQueryParams(request), getRequestHeaders(request), getRequestBody(request, message) }));
			}
		} catch (Exception exp) {
			LOGGER.error("Critical Error in logRequest - ", exp);
		}
	}

	private boolean skipHttpRequest(HttpRequest request) {
		if (skipHttpRequests.isEmpty()) {
			return false;
		} else {
			if (SKIPLIST.contains(request.getURI().toString())) {
				return true;
			} else {
				for (String str : skipHttpRequests) {
					if (request.getURI().toString().toString().indexOf(str) > 0) {
						SKIPLIST.add(request.getURI().toString());
						return true;
					}
				}
			}
		}
		return false;
	}

	private boolean skipHttpRequest(HttpServletRequest request) {
		if (skipHttpRequests.isEmpty()) {
			return false;
		} else {
			if (SKIPLIST.contains(request.getRequestURL().toString())) {
				return true;
			} else {
				for (String str : skipHttpRequests) {
					if (request.getRequestURL().toString().indexOf(str) > 0) {
						SKIPLIST.add(request.getRequestURL().toString());
						return true;
					}
				}
			}
		}
		return false;
	}

	private String getRequestBody(HttpServletRequest reques, String body) {
		if (reques.getContentType() != null && reques.getContentType().equalsIgnoreCase(MediaType.APPLICATION_JSON.toString()) && !StringUtil.isEmpty(body)) {
			return body;
		} else {
			return Constants.DOUBLEQUOTES + body + Constants.DOUBLEQUOTES;
		}
	}

	public void logRequest(HttpRequest request, String message) {
		try {
			if (logRequestFlag.equalsIgnoreCase(Constants.TRUE) && !skipHttpRequest(request)) {
				LOGGER.info(StringUtils.replaceEach(readLogTemplate(LoggerConstants.REQUEST),
						new String[] { LoggerConstants.APPID_DATA, LoggerConstants.COMPONENT_DATA, LoggerConstants.OPERATION_DATA, LoggerConstants.URL_DATA, LoggerConstants.HTTPMETHOD_DATA, LoggerConstants.CONTENTTYPE_DATA, LoggerConstants.REQUESTQUERYPARAMS_DATA,	LoggerConstants.REQUESTHEADERS_JSON, LoggerConstants.REQUESTBODY_JSON },
						new String[] { appId, component, request.getURI().getPath(), request.getURI().toString(), request.getMethod().name(), request.getHeaders().getContentType().toString(),
						getQueryParams(request), getRequestHeaders(request), getRequestBody(request, message) }));
			}
		} catch (Exception exp) {
			LOGGER.error("Critical Error in logRequest - ", exp);
		}
	}

	private String getRequestBody(HttpRequest request, String body) {
		if (request.getHeaders().getContentType() != null && request.getHeaders().getContentType().isCompatibleWith(MediaType.APPLICATION_JSON) && !StringUtil.isEmpty(body)) {
			return body;
		} else {
			return Constants.DOUBLEQUOTES + body + Constants.DOUBLEQUOTES;
		}
	}

	public void logResponse(ContentCachingResponseWrapper response, ContentCachingRequestWrapper request, long elapsedTime) {
		try {
			if (logResponseFlag.equalsIgnoreCase(Constants.TRUE) && !skipHttpResponse(request)) {
				LOGGER.info(StringUtils.replaceEach(readLogTemplate(LoggerConstants.RESPONSE),
						new String[] { LoggerConstants.APPID_DATA, LoggerConstants.COMPONENT_DATA, LoggerConstants.OPERATION_DATA, LoggerConstants.HTTPSTATUSCODE_DATA, LoggerConstants.HTTPSTATUS_DATA, LoggerConstants.ELAPSEDTIME_DATA, LoggerConstants.RESPONSEHEADERS_JSON, LoggerConstants.RESPONSEBODY_JSON },
						new String[] { appId, component, request.getRequestURI(), response.getStatus() + Constants.EMPTY_STRING, HttpStatus.valueOf(response.getStatus()).name(),
								Long.toString(elapsedTime), getResponseHeaders(response), getResponseBody(response) }));
			}
		} catch (Exception exp) {
			LOGGER.error("Critical Error in logResponse - ", exp);
		}
	}

	private boolean skipHttpResponse(ContentCachingRequestWrapper request) {
		if (skipHttpResponses.isEmpty()) {
			return false;
		} else {
			if (SKIPLIST.contains(request.getRequestURL().toString())) {
				return true;
			} else {
				for (String str : skipHttpResponses) {
					if (request.getRequestURL().toString().indexOf(str) > 0) {
						SKIPLIST.add(request.getRequestURL().toString());
						return true;
					}
				}
			}
		}
		return false;
	}

	private String getResponseBody(ContentCachingResponseWrapper response) {
		String body = null;
		try {
			body = IOUtils.toString(response.getContentInputStream(), UTF_8);
		} catch (Exception exp) {
			LOGGER.error("Critical Error in logResponse - ", exp);
		}
		if (response.getContentType() != null && response.getContentType().equalsIgnoreCase(MediaType.APPLICATION_JSON.toString()) && !StringUtil.isEmpty(body)) {
			return body;
		}
		return Constants.DOUBLEQUOTES + body + Constants.DOUBLEQUOTES;
	}

	public void logResponse(ClientHttpResponse response, HttpRequest request, long elapsedTime) {
		try {
			if (logResponseFlag.equalsIgnoreCase(Constants.TRUE) && !skipHttpResponse(request)) {
				LOGGER.info(StringUtils.replaceEach(readLogTemplate(LoggerConstants.RESPONSE),
						new String[] { LoggerConstants.APPID_DATA, LoggerConstants.COMPONENT_DATA, LoggerConstants.OPERATION_DATA, LoggerConstants.HTTPSTATUSCODE_DATA, LoggerConstants.HTTPSTATUS_DATA, LoggerConstants.ELAPSEDTIME_DATA, LoggerConstants.RESPONSEHEADERS_JSON, LoggerConstants.RESPONSEBODY_JSON },
						new String[] { appId, component, request.getURI().getPath(), response.getStatusCode().value() + Constants.EMPTY_STRING, response.getStatusCode().name(), Long.toString(elapsedTime), getResponseHeaders(response), getResponseBody(response) }));
			}
		} catch (Exception exp) {
			LOGGER.error("Critical Error in logResponse - ", exp);
		}
	}

	private boolean skipHttpResponse(HttpRequest request) {
		if (skipHttpResponses.isEmpty()) {
			return false;
		} else {
			if (SKIPLIST.contains(request.getURI().getPath())) {
				return true;
			} else {
				for (String str : skipHttpResponses) {
					if (request.getURI().getPath().indexOf(str) > 0) {
						SKIPLIST.add(request.getURI().getPath());
						return true;
					}
				}
			}
		}
		return false;
	}

	private String getResponseBody(ClientHttpResponse response) {
		String body = null;
		try {
			body = StreamUtils.copyToString(response.getBody(), Charset.defaultCharset());
		} catch (Exception exp) {
			LOGGER.error("Critical Error in logResponse - ", exp);
		}
		if (response.getHeaders().getContentType() != null && response.getHeaders().getContentType().isCompatibleWith(MediaType.APPLICATION_JSON) && !StringUtil.isEmpty(body)) {
			return body;
		}
		return Constants.DOUBLEQUOTES + body + Constants.DOUBLEQUOTES;
	}

	private String getQueryParams(HttpServletRequest request) {
		return request.getQueryString() == null ? Constants.EMPTY_STRING : request.getQueryString();
	}

	private String getQueryParams(HttpRequest request) {
		return request.getURI().getQuery() == null ? Constants.EMPTY_STRING : request.getURI().getQuery();
	}

	private String getRequestHeaders(HttpServletRequest request) {
		try {
			if (reqHeaders == null || reqHeaders.isEmpty()) {
				return Constants.EMPTY_STRING;
			}
			StringBuilder msg = new StringBuilder(Constants.EMPTY_STRING);
			for (String key : reqHeaders) {
				if (request.getHeader(key) != null) {
					msg.append(Constants.DOUBLEQUOTES + key + Constants.DOUBLEQUOTES + Constants.COLON + Constants.DOUBLEQUOTES + request.getHeader(key) + Constants.DOUBLEQUOTES + Constants.COMMA);
				}
			}
			return msg.length() > 0 ? msg.deleteCharAt(msg.length() - 1).toString() : msg.toString();
		} catch (Exception exp) {
			LOGGER.error("Critical Error in getRequestHeaders - ", exp);
		}
		return Constants.EMPTY_STRING;
	}

	private String getRequestHeaders(HttpRequest request) {
		try {
			if (reqHeaders == null || reqHeaders.isEmpty()) {
				return Constants.EMPTY_STRING;
			}
			StringBuilder msg = new StringBuilder(Constants.EMPTY_STRING);
			for (String key : reqHeaders) {
				if (request.getHeaders().containsKey(key)) {
					msg.append(Constants.DOUBLEQUOTES + key + Constants.DOUBLEQUOTES + Constants.COLON + Constants.DOUBLEQUOTES + request.getHeaders().get(key).get(0) + Constants.DOUBLEQUOTES + Constants.COMMA);
				}
			}
			return msg.length() > 0 ? msg.deleteCharAt(msg.length() - 1).toString() : msg.toString();
		} catch (Exception exp) {
			LOGGER.error("Critical Error in getRequestHeaders - ", exp);
		}
		return Constants.EMPTY_STRING;
	}

	private String getResponseHeaders(ContentCachingResponseWrapper response) {
		try {
			if (resHeaders == null || resHeaders.isEmpty()) {
				return Constants.EMPTY_STRING;
			}
			StringBuilder msg = new StringBuilder(Constants.EMPTY_STRING);
			for (String key : resHeaders) {
				if (response.containsHeader(key)) {
					msg.append(Constants.DOUBLEQUOTES + key + Constants.DOUBLEQUOTES + Constants.COLON + Constants.DOUBLEQUOTES + response.getHeader(key) + Constants.DOUBLEQUOTES + Constants.COMMA);
				}
			}
			return msg.length() > 0 ? msg.deleteCharAt(msg.length() - 1).toString() : msg.toString();
		} catch (Exception exp) {
			LOGGER.error("Critical Error in getResponseHeaders - ", exp);
		}
		return Constants.EMPTY_STRING;
	}

	private String getResponseHeaders(ClientHttpResponse response) {
		try {
			if (resHeaders == null || resHeaders.isEmpty()) {
				return Constants.EMPTY_STRING;
			}
			StringBuilder msg = new StringBuilder(Constants.EMPTY_STRING);
			for (String key : resHeaders) {
				if (response.getHeaders().containsKey(key)) {
					msg.append(Constants.DOUBLEQUOTES + key + Constants.DOUBLEQUOTES + Constants.COLON + Constants.DOUBLEQUOTES + response.getHeaders().get(key).get(0) + Constants.DOUBLEQUOTES + Constants.COMMA);
				}
			}
			return msg.length() > 0 ? msg.deleteCharAt(msg.length() - 1).toString() : msg.toString();
		} catch (Exception exp) {
			LOGGER.error("Critical Error in getResponseHeaders - ", exp);
		}
		return Constants.EMPTY_STRING;
	}

	public void logError(Exception exp, String operation) {
		try {
			if (logErrorFlag.equalsIgnoreCase(Constants.TRUE)) {
				if (exp instanceof BusinessException) {
					BusinessException bExp = (BusinessException) exp;
					LOGGER.error(StringUtils.replaceEach(readLogTemplate(LoggerConstants.ERROR_BUSINESS), 
									new String[] { LoggerConstants.APPID_DATA, LoggerConstants.COMPONENT_DATA, LoggerConstants.OPERATION_DATA, LoggerConstants.ERRORCODE_DATA, LoggerConstants.ERRORMESSAGE_DATA },
									new String[] { appId, component, operation, bExp.getCode(), bExp.toString() }));
				} else if (exp instanceof SystemException) {
					SystemException pExp = (SystemException) exp;
					LOGGER.error(StringUtils.replaceEach(readLogTemplate(LoggerConstants.ERROR),
							new String[] { LoggerConstants.APPID_DATA, LoggerConstants.COMPONENT_DATA, LoggerConstants.OPERATION_DATA, LoggerConstants.ERRORCODE_DATA, LoggerConstants.ERRORMESSAGE_DATA, LoggerConstants.ERRORSTACKTRACE_DATA },
							new String[] { appId, component, operation, pExp.getCode(), pExp.toString(), ExceptionUtils.getStackTrace(exp) }));
				} else {
					LOGGER.error(StringUtils.replaceEach(readLogTemplate(LoggerConstants.ERROR),
							new String[] { LoggerConstants.APPID_DATA, LoggerConstants.COMPONENT_DATA, LoggerConstants.OPERATION_DATA, LoggerConstants.ERRORCODE_DATA, LoggerConstants.ERRORMESSAGE_DATA, LoggerConstants.ERRORSTACKTRACE_DATA },
							new String[] { appId, component, operation, CommonErrorCodes.UNKNOWN.getCode(), exp.getMessage(), ExceptionUtils.getStackTrace(exp) }));
				}
			}
		} catch (Exception e) {
			LOGGER.error("Critical Error in logError - ", e);
		}
	}

	public void logInfo(String message, String operation) {
		try {
			if (logInfoFlag.equalsIgnoreCase(Constants.TRUE)) {
				LOGGER.info(StringUtils.replaceEach(readLogTemplate(LoggerConstants.INFO), new String[] { LoggerConstants.APPID_DATA, LoggerConstants.COMPONENT_DATA, LoggerConstants.OPERATION_DATA, LoggerConstants.INFO_DATA },
						new String[] { appId, component, operation, message }));
			}
		} catch (Exception e) {
			LOGGER.error("Critical Error in logInfo - ", e);
		}
	}

	public void logWarn(String message, String operation) {
		try {
			if (logWarnFlag.equalsIgnoreCase(Constants.TRUE)) {
				LOGGER.warn(StringUtils.replaceEach(readLogTemplate(LoggerConstants.WARN), new String[] { LoggerConstants.APPID_DATA, LoggerConstants.COMPONENT_DATA, LoggerConstants.OPERATION_DATA, LoggerConstants.WARN_DATA },
						new String[] { appId, component, operation, message }));
			}
		} catch (Exception e) {
			LOGGER.error("Critical Error in logWarn - ", e);
		}
	}

	public void logDebug(String message, String operation) {
		try {
			if (logDebugFlag.equalsIgnoreCase(Constants.TRUE)) {
				LOGGER.debug(StringUtils.replaceEach(readLogTemplate(LoggerConstants.DEBUG), new String[] { LoggerConstants.APPID_DATA, LoggerConstants.COMPONENT_DATA, LoggerConstants.OPERATION_DATA, LoggerConstants.DEBUG_DATA },
						new String[] { appId, component, operation, message }));
			}
		} catch (Exception e) {
			LOGGER.error("Critical Error in logDebug - ", e);
		}
	}

	private String readFromFile(String logType) {
		if (LOGTEMPLATES.containsKey(logType)) {
			return LOGTEMPLATES.get(logType);
		}
		StringBuilder ret = new StringBuilder(Constants.EMPTY_STRING);
		synchronized (this) {
			String line = null;
			BufferedReader reader = null;
			String filePath = "/logger/template/" + logType.toLowerCase() + ".json";
			try {
				ClassLoader cl = this.getClass().getClassLoader();
				ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(cl);
				Resource[] resources = resolver.getResources("classpath:" + filePath);
				for (Resource resource : resources) {
					reader = new BufferedReader(new InputStreamReader(resource.getInputStream()));
					while ((line = reader.readLine()) != null) {
						ret.append(line);
					}
				}
			} catch (Exception exp) {
				LOGGER.error("Unable to Read File " + filePath, exp);
			} finally {
				if (reader != null) {
					try {
						reader.close();
					} catch (Exception exp) {
						LOGGER.error("Unable to close reader ", exp);
					}
				}
			}
			if (ret.length() > 0) {
				LOGTEMPLATES.put(logType, ret.toString());
			}
		}
		return ret.toString();
	}

	private String readLogTemplate(String logType) {
		if (LOGTEMPLATES.containsKey(logType)) {
			return LOGTEMPLATES.get(logType);
		} else {
			return readFromFile(logType);
		}
	}
}
