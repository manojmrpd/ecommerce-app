package com.squareman.inventory.controller;

import java.util.List;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.squareman.inventory.constants.ResponsePreparer;
import com.squareman.inventory.dto.InventoryDto;
import com.squareman.inventory.service.InventoryService;

@RestController
@RequestMapping("/v1/inventory")
public class InventoryController {

	@Autowired
	InventoryService inventoryService;

	@Autowired
	ResponsePreparer responsePreparer;

	@Autowired
	JobLauncher jobLauncher;

	@Autowired
	Job job;

	@GetMapping("/itemAvailability/{id}")
	public ResponseEntity<InventoryDto> getInventoryBySku(@PathVariable Integer id,
			@RequestHeader(required = false) String correlationId, @RequestHeader(required = false) String clientId) {
		InventoryDto inventory = inventoryService.getInventoryBySku(id);
		if (null != inventory && ObjectUtils.isEmpty(inventory)) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} else {
			return ResponseEntity.ok().headers(responsePreparer.getResponseHttpHeaders(correlationId, clientId))
					.body(inventory);

		}
	}
	
	@PostMapping("/load")
	public ResponseEntity<List<InventoryDto>> loadInventory(@RequestBody List<InventoryDto> inventory){
		List<InventoryDto> list = inventoryService.loadInventory(inventory);
		if (null != list && !list.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} else {
			return ResponseEntity.ok().body(list);
		}
	}

	/*
	 * @GetMapping("/load") public BatchStatus load() throws
	 * JobParametersInvalidException, JobExecutionAlreadyRunningException,
	 * JobRestartException, JobInstanceAlreadyCompleteException {
	 * 
	 * Map<String, JobParameter> maps = new HashMap<>(); maps.put("time", new
	 * JobParameter(System.currentTimeMillis())); JobParameters parameters = new
	 * JobParameters(maps); //JobExecution jobExecution = jobLauncher.run(job,
	 * parameters);
	 * 
	 * System.out.println("JobExecution: " + jobExecution.getStatus());
	 * 
	 * System.out.println("Batch is Running..."); while (jobExecution.isRunning()) {
	 * System.out.println("..."); }
	 * 
	 * return jobExecution.getStatus(); }
	 */

}
