package com.squareman.inventory.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Inventory {
	
	@Id
	private Integer inventoryId;
	private Integer skuId;
	private String skuName;
	private Integer availableQuantity;
	private boolean active;
	private String status;
	private String createdBy;
	private String lastUpdatedBy;

}
