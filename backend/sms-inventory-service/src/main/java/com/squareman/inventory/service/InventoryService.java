package com.squareman.inventory.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.squareman.inventory.dto.InventoryDto;

@Service
public interface InventoryService {

	InventoryDto getInventoryBySku(Integer skuId);

	List<InventoryDto> loadInventory(List<InventoryDto> inventory);

}
