package com.squareman.inventory.batch;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import com.squareman.inventory.entity.Inventory;

@Component
public class Processor implements ItemProcessor<Inventory, Inventory> {

    @Override
    public Inventory process(Inventory inventory) throws Exception {
        return inventory;
    }
}
