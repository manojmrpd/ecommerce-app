package com.squareman.inventory.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.squareman.inventory.dto.InventoryDto;
import com.squareman.inventory.entity.Inventory;
import com.squareman.inventory.exceptions.BusinessException;
import com.squareman.inventory.exceptions.CommonErrorCodes;
import com.squareman.inventory.repository.InventoryRepository;
import com.squareman.inventory.service.InventoryService;

@Service
public class InventoryServiceImpl implements InventoryService {

	@Autowired
	InventoryRepository inventoryRepository;

	@Override
	public InventoryDto getInventoryBySku(Integer skuId) {
		Optional<Inventory> inv = inventoryRepository.findBySkuId(skuId);
		if (null != inv && !ObjectUtils.isEmpty(inv)) {
			Inventory inventory = inv.get();
			ModelMapper mapper = new ModelMapper();
			InventoryDto inventoryDto = mapper.map(inventory, InventoryDto.class);
			return inventoryDto;
		} else {
			throw new BusinessException(CommonErrorCodes.INVENTORY_DATA_NOT_FOUND);
		}
	}

	@Override
	public List<InventoryDto> loadInventory(List<InventoryDto> inventory) {
		List<InventoryDto> inventoryList = new ArrayList<>();
		ModelMapper modelMapper = new ModelMapper();
		for(InventoryDto inventoryDto: inventory) {
			Inventory entity = modelMapper.map(inventoryDto, Inventory.class);
			Inventory dbEntity = inventoryRepository.save(entity);
			InventoryDto inventoryDt = modelMapper.map(dbEntity, InventoryDto.class);
			inventoryList.add(inventoryDt);
		}
		return inventoryList;
	}

}
