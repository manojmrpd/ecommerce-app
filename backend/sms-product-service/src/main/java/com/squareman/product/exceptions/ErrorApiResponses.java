/**
 * 
 */
package com.squareman.product.exceptions;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Retention(RUNTIME)
@Target({ TYPE, METHOD })
@ApiResponses(value = {
		@ApiResponse(code = 422, message = "No data found"),
		@ApiResponse(code = 400, message = "Missing Mandatory headers"),
		@ApiResponse(code = 400, message = "Missing mandatory body"),
		@ApiResponse(code = 405, message = "Missing Mandatory paramters"),
		@ApiResponse(code = 400, message = "Missing Mandatory fields", response = ErrorResponse.class),
		@ApiResponse(code = 422, message = "No data found"),
		@ApiResponse(code = 422, message = "No data found"),
		
})

public @interface ErrorApiResponses {

}
