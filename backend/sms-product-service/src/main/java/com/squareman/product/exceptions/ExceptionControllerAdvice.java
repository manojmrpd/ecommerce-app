package com.squareman.product.exceptions;



import java.sql.SQLDataException;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mapping.MappingException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.fasterxml.jackson.core.JsonParseException;
import com.squareman.product.constants.Constants;
import com.squareman.product.constants.ResponsePreparer;
import com.squareman.product.logging.LoggerService;


@ControllerAdvice
public class ExceptionControllerAdvice extends ResponseEntityExceptionHandler {

	@Autowired
	private LoggerService loggerService;

	@Autowired
	private ResponsePreparer responsePreparer;

	@ExceptionHandler(value = { RuntimeException.class })
	protected ResponseEntity<Object> handleNotFound(final RuntimeException ex, final WebRequest request) {
		HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		ErrorResponse errResp = new ErrorResponse();
		errResp.setCode(httpStatus.value() + Constants.EMPTY_STRING);
		errResp.setMessage(httpStatus.getReasonPhrase());
		loggerService.logError(ex, "handleNotFound");
		return handleExceptionInternal(ex, errResp, responsePreparer.getResponseHttpHeaders(request.getHeader(Constants.CORRELATIONID), request.getHeader(Constants.CLIENTID)), httpStatus, request);
	}

	@ExceptionHandler({ DuplicateKeyException.class })
	public ResponseEntity<Object> handleDuplicateKeyException(DuplicateKeyException ex, final WebRequest request) {
		CommonErrorCodes errorCode = CommonErrorCodes.RECORD_ALREADY_EXIST;
		ErrorResponse errResp = new ErrorResponse();
		errResp.setCode(errorCode.getCode());
		errResp.setMessage(errorCode.getMessage());
		errResp.addErrorsItem(errorCode.getError());
		loggerService.logError(ex, "handleDuplicateKeyException");
		return handleExceptionInternal(ex, errResp, responsePreparer.getResponseHttpHeaders(request.getHeader(Constants.CORRELATIONID), request.getHeader(Constants.CLIENTID)), errorCode.getHttpStatus(), request);
	}

	@ExceptionHandler({ SQLException.class })
	public ResponseEntity<Object> handleSQLException(SQLException ex, final WebRequest request) {
		HttpStatus httpStatus = HttpStatus.SERVICE_UNAVAILABLE;
		ErrorResponse errResp = new ErrorResponse();
		errResp.setCode(httpStatus.value() + Constants.EMPTY_STRING);
		errResp.setMessage(httpStatus.getReasonPhrase());
		loggerService.logError(ex, "handleSQLException");
		return handleExceptionInternal(ex, errResp, responsePreparer.getResponseHttpHeaders(request.getHeader(Constants.CORRELATIONID), request.getHeader(Constants.CLIENTID)), httpStatus, request);
	}

	@ExceptionHandler({ SQLDataException.class })
	public ResponseEntity<Object> handleSQLDataException(SQLDataException ex, final WebRequest request) {
		HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		ErrorResponse errResp = new ErrorResponse();
		errResp.setCode(httpStatus.value() + Constants.EMPTY_STRING);
		errResp.setMessage(httpStatus.getReasonPhrase());
		loggerService.logError(ex, "handleSQLDataException");
		return handleExceptionInternal(ex, errResp, responsePreparer.getResponseHttpHeaders(request.getHeader(Constants.CORRELATIONID), request.getHeader(Constants.CLIENTID)), httpStatus, request);
	}

	@ExceptionHandler({ JsonParseException.class, MappingException.class, NullPointerException.class, IllegalArgumentException.class, IllegalStateException.class,
			Exception.class })
	public ResponseEntity<Object> handleAllException(Exception ex, final WebRequest request) {
		HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		ErrorResponse errResp = new ErrorResponse();
		errResp.setCode(httpStatus.value() + Constants.EMPTY_STRING);
		errResp.setMessage(httpStatus.getReasonPhrase());
		loggerService.logError(ex, "handleAllException");
		return handleExceptionInternal(ex, errResp, responsePreparer.getResponseHttpHeaders(request.getHeader(Constants.CORRELATIONID), request.getHeader(Constants.CLIENTID)), httpStatus, request);
	}

	@ExceptionHandler({ SystemException.class })
	public ResponseEntity<Object> handleSystemException(SystemException ex, final WebRequest request) {
		HttpStatus httpStatus = ex.getErrorCode().getHttpStatus();
		ErrorResponse errResp = new ErrorResponse();
		errResp.setCode(ex.getCode());
		errResp.setMessage(ex.getMessage());
		errResp.setErrors(ex.getErrors());
		loggerService.logError(ex, "handleSystemException");
		return handleExceptionInternal(ex, errResp, responsePreparer.getResponseHttpHeaders(request.getHeader(Constants.CORRELATIONID), request.getHeader(Constants.CLIENTID)), httpStatus, request);
	}

	@ExceptionHandler({ BusinessException.class })
	public ResponseEntity<Object> handleBusinessException(BusinessException ex, final WebRequest request) {
		HttpStatus httpStatus =ex.getErrorCode().getHttpStatus();
		ErrorResponse errResp = new ErrorResponse();
		errResp.setCode(ex.getCode());
		errResp.setMessage(ex.getMessage());
		errResp.setErrors(ex.getErrors());
		loggerService.logError(ex, "handleBusinessException");
		return handleExceptionInternal(ex, errResp, responsePreparer.getResponseHttpHeaders(request.getHeader(Constants.CORRELATIONID), request.getHeader(Constants.CLIENTID)), httpStatus, request);
	}

	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(final HttpMessageNotReadableException ex, final HttpHeaders headers, final HttpStatus status,
			final WebRequest request) {
		HttpStatus httpStatus = HttpStatus.BAD_REQUEST;
		ErrorResponse errResp = new ErrorResponse();
		errResp.setCode(httpStatus.value() + Constants.EMPTY_STRING);
		errResp.setMessage(httpStatus.getReasonPhrase());
		loggerService.logError(ex, "handleHttpMessageNotReadable");
		return handleExceptionInternal(ex, errResp, responsePreparer.getResponseHttpHeaders(request.getHeader(Constants.CORRELATIONID), request.getHeader(Constants.CLIENTID)), httpStatus, request);
	}
}
