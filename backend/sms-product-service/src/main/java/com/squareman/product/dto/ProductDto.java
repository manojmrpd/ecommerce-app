package com.squareman.product.dto;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ProductDto implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String productName;
	private String productDescription;
	private List<SkuInfoDto> skuInfo;
	private BrandDto brand;
	private String sellerName;
	private Date createdTs;
	private String createdBy;
	private Date lastUpdatedTs;
	private String lastUpdatedBy;
	

}
