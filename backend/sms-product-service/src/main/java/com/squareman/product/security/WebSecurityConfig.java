package com.squareman.product.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

import com.squareman.product.filter.JwtTokenValidationFilter;

@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Value("${security.enabled}")
	private boolean isSecurityEnabled;
	
	@Value("{security.unsecureToken}")
	private String unsecureToken;
	

	@Override
    protected void configure(HttpSecurity http) throws Exception {
        http.exceptionHandling().authenticationEntryPoint(new WebAuthenticationEntryPoint())
        .and().cors().and().csrf().disable().requestMatchers().antMatchers("/v1/inventory/**")
        .and().addFilter(new JwtTokenValidationFilter(isSecurityEnabled, unsecureToken))
        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }
	

}
