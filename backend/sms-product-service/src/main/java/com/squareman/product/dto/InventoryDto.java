package com.squareman.product.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class InventoryDto {
	
	private Integer inventoryId;
	private Integer skuId;
	private String skuName;
	private Integer availableQuantity;
	private boolean active;
	private String status;
	private String createdBy;
	private String lastUpdatedBy;


}
