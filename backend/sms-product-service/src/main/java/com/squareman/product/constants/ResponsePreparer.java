package com.squareman.product.constants;

import java.time.LocalDateTime;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class ResponsePreparer {
	
	@Value("${spring.application.profile")
	private String appProfile;
	
	@Value("${spring.application.name")
	private String service;

	public org.springframework.http.HttpHeaders getResponseHttpHeaders(String correlationId, String clientId) {
		org.springframework.http.HttpHeaders responseHeaders = new org.springframework.http.HttpHeaders();
		if(StringUtils.isEmpty(correlationId)) {
			responseHeaders.set(Constants.CORRELATIONID, UUID.randomUUID().toString());
		} else {
			responseHeaders.set(Constants.CORRELATIONID, correlationId);
		}
		responseHeaders.set(Constants.CLIENTID, clientId);
		responseHeaders.set(Constants.RESPONSETIMESTAMP, LocalDateTime.now().toString());
		responseHeaders.set(Constants.SERVICENAME, service);
		responseHeaders.set(Constants.SOURCE, appProfile);
		return responseHeaders;
	}

}
