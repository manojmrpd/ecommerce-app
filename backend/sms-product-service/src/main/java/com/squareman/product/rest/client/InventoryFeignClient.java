package com.squareman.product.rest.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.squareman.product.dto.InventoryDto;
import com.squareman.product.rest.client.config.InventoryClientConfig;

@FeignClient(name="inventoryClient", url = "${sms.client.inventory.baseUrl}", configuration = InventoryClientConfig.class)
public interface InventoryFeignClient {
	
	@GetMapping(value = "/v1/inventory/itemAvailability/{skuId}")
	public InventoryDto getItemAvailability(@PathVariable("skuId") Integer skuId);

}
