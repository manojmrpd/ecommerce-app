package com.squareman.product.rest.client;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class TokenData {
	
	@JsonProperty("access_token")
	private String accessToken;
	
	@JsonProperty("id_token")
	private String idToken;
	
	@JsonProperty("expires_in")
	private long expiresIn;
	
	@JsonProperty("token_type")
	private String tokenType;

}
