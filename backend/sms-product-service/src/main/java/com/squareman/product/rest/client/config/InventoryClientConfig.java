package com.squareman.product.rest.client.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.squareman.product.rest.client.KeyclockAuthorizationService;
import com.squareman.product.rest.client.error.InventoryErrorHandler;

import feign.Logger;
import feign.RequestInterceptor;

@Component
public class InventoryClientConfig {
	
	@Autowired
	private KeyclockAuthorizationService keyClockAuthorizationService;
	
	@Bean
	public Logger.Level feignLoggerLevel() {
		return Logger.Level.FULL;
	}
	
	@Bean
	public InventoryErrorHandler feignErrorHandler() {
		return new InventoryErrorHandler();
	}
	
	@Bean
	public RequestInterceptor keyClockRequestInterceptor() {
		return requestTemplate -> {
			requestTemplate.header("Authorization", "Bearer "+keyClockAuthorizationService.getIdToken());
		};
	}

}
