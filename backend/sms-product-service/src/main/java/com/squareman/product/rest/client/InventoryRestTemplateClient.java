package com.squareman.product.rest.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.web.client.RestTemplate;

import com.squareman.product.constants.Constants;
import com.squareman.product.dto.InventoryDto;
import com.squareman.product.exceptions.BusinessException;
import com.squareman.product.exceptions.CommonErrorCodes;

@Component
public class InventoryRestTemplateClient {

	@Autowired
	RestTemplate restTemplate;
	
	@Value("${sms.inventory.itemAvailability.url}")
	private String url;

	public InventoryDto getItemAvailability(Integer skuId) {

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.add("clientId", Constants.CLIENTID);
		requestHeaders.add("correlationId", Constants.CORRELATIONID);
		HttpEntity<InventoryDto> requestEntity = new HttpEntity<>(requestHeaders);

		String inventoryUrl = url+skuId;
		ResponseEntity<InventoryDto> response = restTemplate.exchange(inventoryUrl, HttpMethod.GET, requestEntity, InventoryDto.class);
		
		if (null != response && !ObjectUtils.isEmpty(response)) {
			InventoryDto inventory = response.getBody();
			return inventory;
		} else {
			throw new BusinessException(CommonErrorCodes.INTERNAL_SERVER_ERROR);
		}
	}

}
