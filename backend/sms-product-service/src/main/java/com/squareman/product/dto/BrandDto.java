package com.squareman.product.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class BrandDto implements Serializable {	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String brandName;

}
