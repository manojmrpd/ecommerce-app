package com.squareman.product.controller;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.squareman.product.constants.Constants;
import com.squareman.product.constants.ResponsePreparer;
import com.squareman.product.dto.ProductDto;
import com.squareman.product.service.ProductService;

@RestController
@RequestMapping("/v1")
public class ProductController {
	
	@Autowired
	ProductService productService;
	
	@Autowired
	ResponsePreparer responsePreparer;
	
	@PostMapping("/products")
	public ResponseEntity<ProductDto> newProducts(@RequestBody ProductDto productDto, @RequestHeader(value = Constants.CORRELATIONID, required = true) String correlationId,
			@RequestHeader(value = Constants.CLIENTID, required = true) String clientId) {
		ProductDto product = productService.newProducts(productDto);
		URI location = ServletUriComponentsBuilder
				.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(product.getProductName())
				.toUri();
		return ResponseEntity.created(location).headers(responsePreparer.getResponseHttpHeaders(correlationId, clientId)).body(product);
	}
	
	
	@PostMapping("/products/{id}")
	public ResponseEntity<ProductDto> getProductDetails(@PathVariable Integer id, @RequestHeader(value = Constants.CORRELATIONID, required = true) String correlationId,
			@RequestHeader(value = Constants.CLIENTID, required = true) String clientId){
		ResponseEntity<ProductDto> response = null;
		ProductDto product = productService.getProducts(id);
		if(null!=product && !ObjectUtils.isEmpty(product)) {
			return ResponseEntity.ok().headers(responsePreparer.getResponseHttpHeaders(correlationId, clientId)).body(product);
		} else {
			new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return response;
	}

}
