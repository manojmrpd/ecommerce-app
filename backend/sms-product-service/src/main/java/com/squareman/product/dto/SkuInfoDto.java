package com.squareman.product.dto;

import java.io.Serializable;
import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SkuInfoDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String skuName;
	private String skuDescription;
	private String imageURL;
	private PriceInfoDto priceInfo;
	private Date createdTs;
	private String createdBy;
	private Date lastUpdatedTs;
	private String lastUpdatedBy;
	private InventoryDto inventory;

}
