package com.squareman.product.filter;

import java.io.IOException;
import java.text.ParseException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import com.nimbusds.jwt.JWT;
import com.nimbusds.jwt.JWTParser;
import com.squareman.product.exceptions.BusinessException;
import com.squareman.product.exceptions.CommonErrorCodes;

public class JwtTokenValidationFilter extends BasicAuthenticationFilter {

	private static final String HEADER_NAME = "x-auth-token";

	private final boolean isSecurityEnabled;

	private final String unsecureToken;

	public JwtTokenValidationFilter(boolean isSecurityEnabled, String unsecureToken) {
		super(new InventoryAuthenticationManager());
		this.isSecurityEnabled = isSecurityEnabled;
		this.unsecureToken = unsecureToken;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws IOException, ServletException {

		if (SecurityContextHolder.getContext() != null && SecurityContextHolder.getContext().getAuthentication() != null
				&& SecurityContextHolder.getContext().getAuthentication().isAuthenticated()) {
			filterChain.doFilter(request, response);
			return;
		}

		UsernamePasswordAuthenticationToken authentication = getAuthentication(request);

		if (authentication == null) {
			filterChain.doFilter(request, response);
			return;
		}
		SecurityContextHolder.getContext().setAuthentication(authentication);
		filterChain.doFilter(request, response);
	}

	private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
		String token = request.getHeader(HEADER_NAME);
		if (token != null) {
			try {
				JWT jwt = JWTParser.parse(token);
				return new UsernamePasswordAuthenticationToken(jwt, null);
			} catch (ParseException e) {
				throw new BusinessException(CommonErrorCodes.UNAUTHORIZED);
			}

		} else if (isSecurityEnabled) {
			throw new BusinessException(CommonErrorCodes.UNAUTHORIZED);
		} else {
			try {
				JWT jwt = JWTParser.parse(unsecureToken);
				return new UsernamePasswordAuthenticationToken(jwt, null);
			} catch (ParseException e) {
				throw new BusinessException(CommonErrorCodes.UNAUTHORIZED);
			}
		}
	}

	private static class InventoryAuthenticationManager implements AuthenticationManager {
		@Override
		public Authentication authenticate(Authentication authentication) throws AuthenticationException {
			throw new UnsupportedOperationException("No Authentication should be done with AuthenticationManager");
		}
	}
}
