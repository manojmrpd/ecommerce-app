package com.squareman.product.rest.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.squareman.product.logging.LoggerService;

@Component
public class KeyclockAuthorizationService {
	
	@Value("${keyclock.scopeAudience}")
	private String scopeAudience;
	
	@Autowired
	private LoggerService loggerService;
	
	@Value("${keyclock.clientId}")
	private String clientId;
	
	@Value("${keyclock.clientSecret}")
	private String clientSecret;
	
	@Value("${keyclock.baseUrl}")
	private String url;
	
	@Bean
	RestTemplate getRestTemplate() {
		return new RestTemplate();
	}
	
	public String getIdToken() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
		map.add("grant_type", "client_credentials");
		map.add("scope", "openid profile");
		map.add("state", "profile");
		map.add("client_id", clientId);
		map.add("client_secret", clientSecret);
		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
		ResponseEntity<TokenData> response = getRestTemplate().postForEntity(url+"/token", request , TokenData.class);
		TokenData tokenData = response.getBody();
		String accessToken = tokenData.getAccessToken();
		loggerService.logInfo("Token Info:: ", tokenData.toString());
		return accessToken;
	}	
}
