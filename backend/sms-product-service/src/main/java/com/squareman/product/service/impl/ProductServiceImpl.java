package com.squareman.product.service.impl;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.squareman.product.dto.InventoryDto;
import com.squareman.product.dto.ProductDto;
import com.squareman.product.entity.Product;
import com.squareman.product.entity.SkuInfo;
import com.squareman.product.exceptions.BusinessException;
import com.squareman.product.exceptions.CommonErrorCodes;
import com.squareman.product.mapper.impl.ProductMapper;
import com.squareman.product.repository.ProductRepository;
import com.squareman.product.rest.client.InventoryFeignClient;
import com.squareman.product.service.ProductService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductRepository productRepository;

	@Autowired
	ProductMapper productMapper;
	
	@Autowired
	InventoryFeignClient inventoryFeignClient;

	@Override
	public ProductDto newProducts(ProductDto productDto) {

		Product product = productMapper.mapProductInfo(productDto);
		Product savedProduct = productRepository.save(product);
		ModelMapper mapper = new ModelMapper();
		ProductDto productInfo = mapper.map(savedProduct, ProductDto.class);
		return productInfo;
	}

	@Override
	public ProductDto getProducts(Integer id) {
		Optional<Product> products = productRepository.findById(id);
		if (null != products && !ObjectUtils.isEmpty(products)) {
			Product product = products.get();
			List<SkuInfo> skuInfo = product.getSkuInfo();
			for(SkuInfo sku: skuInfo) {
				Integer skuId = sku.getSkuId();
				InventoryDto inventory = inventoryFeignClient.getItemAvailability(skuId);
				log.info("The Inventory available is: "+inventory.toString());
			}
			ModelMapper mapper = new ModelMapper();
			ProductDto productInfo = mapper.map(product, ProductDto.class);
			return productInfo;
		} else {
			throw new BusinessException(CommonErrorCodes.PRODUCT_DATA_NOT_FOUND);
		}
	}
}
