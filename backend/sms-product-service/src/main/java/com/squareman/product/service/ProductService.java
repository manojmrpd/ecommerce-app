package com.squareman.product.service;

import org.springframework.stereotype.Service;

import com.squareman.product.dto.ProductDto;

@Service
public interface ProductService {

	ProductDto newProducts(ProductDto productDto);

	ProductDto getProducts(Integer id);

}
