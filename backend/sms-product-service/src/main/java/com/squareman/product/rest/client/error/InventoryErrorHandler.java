package com.squareman.product.rest.client.error;

import org.springframework.web.server.ServerErrorException;

import com.squareman.product.constants.Constants;
import com.squareman.product.exceptions.BusinessException;
import com.squareman.product.exceptions.CommonErrorCodes;

import feign.Response;
import feign.codec.ErrorDecoder;

public class InventoryErrorHandler implements ErrorDecoder {

	@SuppressWarnings("deprecation")
	@Override
	public Exception decode(String methodKey, Response response) {
		if(response.status() == Constants.UNPROCESSABLE_ENTITY) {
			throw new BusinessException(CommonErrorCodes.SERVICE_UNAVAILABLE);
			
		}
		throw new ServerErrorException(String.format("Inventory Respponse: %d %s: %s", response.status(), response.reason(), response.body()));
	}

}
