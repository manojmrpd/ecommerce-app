package com.squareman.profile.client.store.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
/**
 * 
 * @author Manoj Vurukonda
 *
 */
@Getter
@Setter
@Data
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor	
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class AddressDetail {
	
	@JsonProperty("city")
	private String city;
	@JsonProperty("street")
	private String street;
	@JsonProperty("number")
	private Integer number;
	@JsonProperty("zipcode")
	private String zipcode;
	@JsonProperty("geolocation")
	private GeoLocation geolocation;
	

}
