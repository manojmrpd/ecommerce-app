package com.squareman.profile.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.squareman.profile.dto.UserDto;
/**
 * 
 * @author Manoj Vurukonda
 *
 */
@Service
public interface UserProfileService {

	/**
	 * Get All User Details.
	 * @return user
	 */
	List<UserDto> getUserDetails();
	/**
	 * Create a New User.
	 * @param userDto
	 * @return user
	 */
	UserDto createUser(UserDto userDto);

}
