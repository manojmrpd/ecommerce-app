package com.squareman.profile.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.squareman.profile.dto.ERole;
import com.squareman.profile.dto.JwtResponse;
import com.squareman.profile.dto.LoginRequest;
import com.squareman.profile.dto.MessageResponse;
import com.squareman.profile.dto.SignupRequest;
import com.squareman.profile.entity.Role;
import com.squareman.profile.entity.User;
import com.squareman.profile.repository.RoleRepository;
import com.squareman.profile.repository.UserRepository;
import com.squareman.profile.security.jwt.JwtUtils;
import com.squareman.profile.service.impl.UserDetailsImpl;

import io.swagger.annotations.ApiOperation;

/**
 * 
 * @author Manoj Vurukonda
 *
 */
@RestController
@RequestMapping("/api/auth")
public class UserAuthController {
	/**
	 * User Repository.
	 */
	@Autowired
	UserRepository userRepository;
	/**
	 * Role Repository.
	 */
	@Autowired
	RoleRepository roleRepository;
	/**
	 * Password Encoder.
	 */
	@Autowired
	PasswordEncoder encoder;

	@Autowired
	AuthenticationManager authenticationManager;
	/**
	 * JWT Utils.
	 */
	@Autowired
	JwtUtils jwtUtils;

	/**
	 * Register User.
	 * @param signUpRequest
	 * @return reponse
	 */
	@ApiOperation(value = "register new user account")
	@PostMapping("/signup")
	public ResponseEntity<MessageResponse> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
		if (userRepository.existsByUsername(signUpRequest.getUsername())) {
			return ResponseEntity.badRequest().body(new MessageResponse("Error: Username is already taken!"));
		}
		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return ResponseEntity.badRequest().body(new MessageResponse("Error: Email is already in use!"));
		}
		// Create new user's account
		User user = new User(signUpRequest.getUsername(), encoder.encode(signUpRequest.getPassword()),
				signUpRequest.getFirstName(), signUpRequest.getLastName(), signUpRequest.getEmail(),
				signUpRequest.getPhone());

		Set<String> strRoles = signUpRequest.getRole();
		Set<Role> roles = new HashSet<>();
		if (strRoles == null) {
			Role userRole = roleRepository.findByName(ERole.user)
					.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
			roles.add(userRole);
		} else {
			strRoles.forEach(role -> {
				switch (role) {
				case "admin":
					Role adminRole = roleRepository.findByName(ERole.admin)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(adminRole);

					break;
				case "mod":
					Role modRole = roleRepository.findByName(ERole.moderator)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(modRole);

					break;
				default:
					Role userRole = roleRepository.findByName(ERole.user)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(userRole);
				}
			});
		}
		user.setRoles(roles);
		userRepository.save(user);
		return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
	}

	/**
	 * Authenticate User.
	 * @param loginRequest
	 * @return jwt reponse
	 */
	@ApiOperation(value = "authenticate the existing user while login using jwt")
	@PostMapping("/signin")
	public ResponseEntity<JwtResponse> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);
		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
		List<String> roles = userDetails.getAuthorities().stream().map(item -> item.getAuthority())
				.collect(Collectors.toList());
		return ResponseEntity.ok(
				new JwtResponse(jwt, userDetails.getId(), userDetails.getUsername(), userDetails.getEmail(), roles));
	}
}
