package com.squareman.profile.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.squareman.profile.entity.User;
/**
 * 
 * @author Manoj Vurukonda
 *
 */
public interface UserRepository extends JpaRepository<User, Long> {
	/**
	 * Find User by User Name.
	 * @param userName
	 * @return user
	 */
	Optional<User> findByUsername(String userName);
	/**
	 * Find User By Email.
	 * @param email
	 * @return user
	 */
	User findByEmail(String email);
	/**
	 * Find all users.
	 */
	List<User> findAll();
	/**
	 * Check if user exists by username.
	 * @param username
	 * @return boolean value
	 */
	Boolean existsByUsername(String username);
	/**
	 * Check if user exists by email.
	 * @param email
	 * @return boolean value
	 */
	Boolean existsByEmail(String email);

}
