package com.squareman.profile.client.store;

import org.springframework.context.annotation.Bean;

import feign.Logger;
import feign.RequestInterceptor;
/**
 * 
 * @author Manoj Vurukonda
 *
 */
public class XStoreClientConfig {

	@Bean
	public Logger.Level feignLoggerLevel() {
		return Logger.Level.FULL;
	}
	
	@Bean
	public XStoreClientErrorHandler feignErrorHandler() {
		return new XStoreClientErrorHandler();
	}
	
	@Bean
	public RequestInterceptor requestInterceptor() {
		return requestTemplate -> {
			requestTemplate.header("contentType", "application/json");
		};
	}
}
