package com.squareman.profile.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.squareman.profile.dto.ERole;
import com.squareman.profile.entity.Role;
/**
 * 
 * @author Manoj Vurukonda
 *
 */
@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
	/**
	 * Find Role By Name.
	 * @param name
	 * @return role
	 */
	Optional<Role> findByName(ERole name);
}
