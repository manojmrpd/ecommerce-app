package com.squareman.profile.controller;

import java.net.URI;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.squareman.profile.constants.Constants;
import com.squareman.profile.constants.ResponsePreparer;
import com.squareman.profile.dto.UserDto;
import com.squareman.profile.exceptions.ErrorApiResponses;
import com.squareman.profile.service.UserProfileService;
import com.squareman.profile.validator.UserProfileRequestValidator;

import io.swagger.annotations.ApiOperation;

/**
 * 
 * @author Manoj Vurukonda
 *
 */
@RequestMapping("/v1")
@RestController
public class UserProfileController {

	/**
	 * LOGGER.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(UserProfileController.class);
	/**
	 * User Profile Service.
	 */
	@Autowired
	private UserProfileService userProfileService;

	/**
	 * Response Preparer.
	 */
	@Autowired
	private ResponsePreparer responsePreparer;
	/**
	 * Create a New User in MySql Database.
	 * @param userDto
	 * @param correlationId
	 * @param clientId
	 * @return user
	 */
	@ApiOperation(value = "Create a New User in MySql Database")
	@PostMapping(value = "/createUser", consumes = "application/json")
	public ResponseEntity<UserDto> createUser(@RequestBody(required = true) UserDto userDto,
			@RequestHeader(value = Constants.CORRELATIONID, required = true) String correlationId,
			@RequestHeader(value = Constants.CLIENTID, required = true) String clientId) {
		UserProfileRequestValidator.validateCreateUserRequest(userDto);
		UserDto userInfo = userProfileService.createUser(userDto);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(userInfo.getId())
				.toUri();
		return ResponseEntity.created(location)
				.headers(responsePreparer.getResponseHttpHeaders(correlationId, clientId)).body(userInfo);

	}
	/**
	 * Get All the User Details from MySql Database.
	 * @param correlationId
	 * @param clientId
	 * @return list of users
	 */
	@ApiOperation(value = "Get All the Users Details")
	@ErrorApiResponses
	@GetMapping(value = "/userDetails", produces = "application/json")
	public ResponseEntity<List<UserDto>> getUserDetails(
			@RequestHeader(value = Constants.CORRELATIONID, required = true) String correlationId,
			@RequestHeader(value = Constants.CLIENTID, required = true) String clientId) {
		ResponseEntity<List<UserDto>> response = null;
		List<UserDto> userDetails = userProfileService.getUserDetails();
		if (null != userDetails && !userDetails.isEmpty()) {
			return ResponseEntity.ok().headers(responsePreparer.getResponseHttpHeaders(correlationId, clientId))
					.body(userDetails);
		} else {
			new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return response;
	}
}
