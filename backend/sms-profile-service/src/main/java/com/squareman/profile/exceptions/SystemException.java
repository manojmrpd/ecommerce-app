package com.squareman.profile.exceptions;

import java.util.ArrayList;
import java.util.List;

public class SystemException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String code;
	private final ErrorCodeIF errorCode;
	private final List<Error> errors;
	
	public SystemException(ErrorCodeIF errorCode) {
		super(errorCode.getMessage());
		this.code = errorCode.getCode();
		this.errors = new ArrayList<>();
		this.errors.add(errorCode.getError());
		this.errorCode = errorCode;
	}
	
	public SystemException(ErrorCodeIF errorCode, List<Error> errors) {
		super(errorCode.getMessage());
		this.code = errorCode.getCode();
		this.errors = errors;
		this.errorCode = errorCode;
	}

	public String getCode() {
		return code;
	}

	public ErrorCodeIF getErrorCode() {
		return errorCode;
	}

	public List<Error> getErrors() {
		return errors;
	}

	@Override
	public String toString() {
		return "SystemException [code=" + code + ", errorCode=" + errorCode + ", errors=" + errors + "]";
	}

}
