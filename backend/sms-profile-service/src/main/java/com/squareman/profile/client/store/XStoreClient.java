package com.squareman.profile.client.store;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;

import com.squareman.profile.client.store.dto.UserDetail;
/**
 * 
 * @author Manoj Vurukonda
 *
 */
@FeignClient(name = "xStoreClient", url = "${client.xstore.baseUrl}", configuration = XStoreClientConfig.class)
public interface XStoreClient {

	@GetMapping(value = "/users", consumes = MediaType.APPLICATION_JSON_VALUE)
	List<UserDetail> getAllUsers();
}
