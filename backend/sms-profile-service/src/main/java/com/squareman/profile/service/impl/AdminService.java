package com.squareman.profile.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.squareman.profile.client.store.XStoreClient;
import com.squareman.profile.client.store.dto.UserDetail;
import com.squareman.profile.dto.UserInfo;
import com.squareman.profile.entity.Admin;
import com.squareman.profile.repository.AdminRespository;
@Service
public class AdminService {
	/**
	 * Admin Repository.
	 */
	@Autowired
	AdminRespository adminRepository;
	/**
	 * XStore Client
	 */
	@Autowired
	XStoreClient xStoreClient;
	/**
	 * LOGGER.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(AdminService.class);

	/**
	 * Create new user info in azure cloud sql database.
	 * @param userInfo
	 * @return user
	 */
	public UserInfo createNewUser(UserInfo userInfo) {
		LOGGER.info("Entering in to createNewUser in Adminservice: "+userInfo.toString());
		Admin admin = new Admin();
		admin.setUserName(userInfo.getUserName());
		admin.setEmail(userInfo.getUserName());
		admin.setFirstName(userInfo.getFirstName());
		admin.setLastName(userInfo.getLastName());
		Admin dbUserData = adminRepository.save(admin);
		LOGGER.info("Azure Cloud SQL DB user data is : "+dbUserData.toString());
		return userInfo;
	}

	/**
	 * Get all users from X-Store Client.
	 * @return all users.
	 */
	public List<UserDetail> getAllUsers() {
		LOGGER.info("Entering in to getAllUsers in Adminservice: ");
		List<UserDetail> allUsers = xStoreClient.getAllUsers();
		LOGGER.info("XStore API all users data from feign client is : "+allUsers.toString());
		return allUsers;
	}
}
