package com.squareman.profile.constants;
/**
 * 
 * @author Manoj Vurukonda
 *
 */
public class Constants {
	
	public static final String PUBLISH_TIME = "publishTime";
	public static final String CORRELATIONID = "correlationId";
	public static final String CLIENTID = "clientId";
	public static final String RESPONSETIMESTAMP = "responseTimestamp";
	public static final String SOURCE = "source";
	public static final String SERVICENAME = "serviceName";
	public static final String APPNAME_KEY = "appName";
	public static final String ERRORCODE = "errorCode";
	public static final String EMPTY_STRING = "";
	public static final String TRUE = "TRUE";
	public static final String FALSE = "FALSE";
	public static final String ONE_SPACE = " ";
	public static final String FORWARDSLASH = "/";
	public static final String DOUBLEQUOTES = "\"";
	public static final String COLON = ":";
	public static final String SEMICOLON = ";";
	public static final String COMMA = ",";
	public static final String YES = "Y";
	public static final String NO = "N";
	public static final String HYPHEN = "-";
	public static final char RIGHT_CURLY_BRACKET = '{';
	public static final char LEFT_CURLY_BRACKET = '}';
	public static final String DATE_FORMAT1 = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";
	public static final String ADD="ADD";
	public static final String UPDATE="UPDATE";
	public static final String DELETE="DELETE";
	public static final String OPEN_BRACKET="[";
	public static final String CLOSE_BRACKET="]";
	public static final String CACHE_LOCATION_DIV_STORENUMBER = "lconByDivStoreNumber";
	public static final String CACHE_LOCATION_LOCNNUMBER = "lconByLocnNumber";
	public static final String CACHE_METADATA_BUSEVENT_TXNTYPE= "metadataByBusEventTxnType";
	public static final String ELAPSED_TIME_MESSAGE = "Elapsed Time Exception : ";
	public static final String TIE_BREAKER_TYPE_ALL = "ALL";
	public static final String TIE_BREAKER_TYPE_OUTING = "OUT_IO";
	public static final String DIVISION_NBR = "divisionNbr";
	public static final String SELLING_CHANNEL = "sellingChannelType";
	public static final String RETRYCOUT = "retryCount";
	public static final String RETRYDATA = "retryData";
	public static final String REQUESTTIME = "requestTime";
}
