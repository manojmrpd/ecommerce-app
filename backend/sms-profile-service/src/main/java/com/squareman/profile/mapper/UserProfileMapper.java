package com.squareman.profile.mapper;

import java.util.List;

import org.springframework.stereotype.Component;

import com.squareman.profile.dto.UserDto;
import com.squareman.profile.entity.User;

/**
 * @author Manoj Vurukonda
 */
@Component
public interface UserProfileMapper {
	/**
	 * Get DTO From Model.
	 * @param userDetails
	 * @return
	 */
	List<UserDto> getDTOFromModel(List<User> userDetails);
	/**
	 * Get Model from DTO.
	 * @param userDto
	 * @return
	 */
	User getModelFromDTO(UserDto userDto);
	/**
	 * Get DTO From Model.
	 * @param user
	 * @return
	 */
	UserDto getDTOFromModel(User user);
}
