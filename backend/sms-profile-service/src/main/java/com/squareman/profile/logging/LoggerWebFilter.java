package com.squareman.profile.logging;


import java.io.IOException;
import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.CommonsRequestLoggingFilter;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;

import com.google.common.base.Stopwatch;
import com.squareman.profile.constants.Constants;
/**
 * 
 * @author Manoj Vurukonda
 *
 */
@Component
public class LoggerWebFilter extends CommonsRequestLoggingFilter {

	@Autowired
	private LoggerService loggerService;

	@Value("${logging.request.size:10000}")
	private int requestSize;
	
	@Override
	protected void beforeRequest(HttpServletRequest request, String message) {
		request.setAttribute(Constants.REQUESTTIME, LocalDateTime.now().toString());
		setMaxPayloadLength(requestSize);
		setIncludePayload(true);
		super.beforeRequest(request, message);
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
		try {
			MDC.put(Constants.CORRELATIONID, request.getHeader(Constants.CORRELATIONID));
			Stopwatch startTime = Stopwatch.createStarted();
			ContentCachingRequestWrapper requestWrapper = new ContentCachingRequestWrapper(request);
			ContentCachingResponseWrapper responseWrapper = new ContentCachingResponseWrapper(response);
			super.doFilterInternal(requestWrapper, responseWrapper, filterChain);
			long elapsedTime = startTime.elapsed(TimeUnit.MILLISECONDS);
			loggerService.logResponse(responseWrapper, requestWrapper, elapsedTime);
			responseWrapper.copyBodyToResponse();
		} finally {
			MDC.remove(Constants.CORRELATIONID);
		}
	}

	@Override
	protected boolean shouldLog(HttpServletRequest request) {
		return true;
	}

	@Override
	protected void afterRequest(HttpServletRequest request, String message) {
		loggerService.logRequest(request, message);
		super.afterRequest(request, message);
	}

	@Override
	protected String createMessage(HttpServletRequest request, String prefix, String suffix) {
		StringBuilder msg = new StringBuilder();
		if (isIncludePayload()) {
			msg.append(getMessagePayload(request));
		}
		return msg.toString();
	}
}
