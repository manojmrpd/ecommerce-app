package com.squareman.profile.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.squareman.profile.entity.Admin;
/**
 * @author Manoj Vurukonda
 */
@Repository
public interface AdminRespository extends CrudRepository<Admin, Integer> {
}
