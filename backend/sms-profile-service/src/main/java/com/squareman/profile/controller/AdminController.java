package com.squareman.profile.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.squareman.profile.client.store.dto.UserDetail;
import com.squareman.profile.dto.UserInfo;
import com.squareman.profile.service.impl.AdminService;

import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.parameters.RequestBody;

@RestController
@RequestMapping("admin/v1")
public class AdminController {

	@Autowired
	AdminService adminService;

	@Autowired
	private static final Logger LOGGER = LoggerFactory.getLogger(AdminController.class);

	/**
	 * 	Created a new user info in azure cloud sql database.
	 * @param userInfo
	 * @return userInfo
	 * @throws Exception
	 */
	@ApiOperation(value = "create a users information in azure sql database")
	@PostMapping(value = "/user", consumes = "application/json", produces = "application/json")
	ResponseEntity<UserInfo> createNewUserInfo(@RequestBody UserInfo userInfo) throws Exception {
		LOGGER.info("Entering createNewUserInfo method ");
		UserInfo response = adminService.createNewUser(userInfo);
		return ResponseEntity.ok(response);

	}

	/**
	 * Get all the user info from X-Store client application.
	 * @return list of the users
	 * @throws Exception
	 */
	@ApiOperation(value = "get all store users information from xstore feign client")
	@GetMapping(value = "/allUsers", produces = "application/json")
	ResponseEntity<List<UserDetail>> getAllUsersInfo() throws Exception {
		LOGGER.info("Entering getAllUsersInfo method ");
		List<UserDetail> response = adminService.getAllUsers();
		return ResponseEntity.ok(response);
	}
}
