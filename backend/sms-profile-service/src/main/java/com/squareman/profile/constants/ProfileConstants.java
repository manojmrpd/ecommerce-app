package com.squareman.profile.constants;
/**
 * 
 * @author Manoj Vurukonda
 *
 */
public class ProfileConstants {

	public static final String ID = "id";
	public static final String FIRSTNAME = "firstName";
	public static final String LASTNAME = "lastName";
	public static final String USERNAME = "username";
	public static final String EMAIL = "email";
	public static final String PHONE = "phone";

}
