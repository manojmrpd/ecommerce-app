package com.squareman.profile.dto;

public enum ERole {
	user,
    moderator,
    admin
}
