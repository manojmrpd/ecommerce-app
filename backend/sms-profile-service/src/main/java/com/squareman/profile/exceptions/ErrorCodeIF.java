package com.squareman.profile.exceptions;

import org.springframework.http.HttpStatus;

public interface ErrorCodeIF {
	
	public String getCode();
	public String getMessage();
	public HttpStatus getHttpStatus();
	public Error getError(String... customMessage);

}
