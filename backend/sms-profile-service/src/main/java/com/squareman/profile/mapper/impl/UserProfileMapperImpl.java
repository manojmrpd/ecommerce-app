package com.squareman.profile.mapper.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.squareman.profile.dto.UserDto;
import com.squareman.profile.entity.User;
import com.squareman.profile.mapper.UserProfileMapper;

/**
 * @author Manoj Vurukonda
 */
@Component
public class UserProfileMapperImpl implements UserProfileMapper {
	/**
	 * Map UserDto from User entity.
	 * @param user entity.
	 * @return user dto
	 */
	public List<UserDto> getDTOFromModel(List<User> userDetails) {
		if (userDetails == null) {
			return null;
		}
		List<UserDto> list = new ArrayList<UserDto>(userDetails.size());
		for (User user : userDetails) {
			list.add(getUserDto(user));
		}
		return list;
	}

	/**
	 * Map UserDto from User entity.
	 * @param user
	 * @return userDto
	 */
	private UserDto getUserDto(User user) {
		if (user == null) {
			return null;
		}
		UserDto userDto = new UserDto();
		userDto.setId(user.getId());
		userDto.setUsername(user.getUsername());
		userDto.setFirstName(user.getFirstName());
		userDto.setLastName(user.getLastName());
		userDto.setPassword(user.getPassword());
		userDto.setEmail(user.getEmail());
		userDto.setPhone(user.getPhone());
		return userDto;
	}

	/**
	 * Map user entity from UserDto.
	 * @param userDto
	 * @return user
	 */
	@Override
	public User getModelFromDTO(UserDto userDto) {
		if (userDto == null) {
			return null;
		}
		User user = new User();
		user.setId(userDto.getId());
		user.setFirstName(userDto.getFirstName());
		user.setLastName(userDto.getLastName());
		user.setUsername(userDto.getLastName());
		user.setPassword(userDto.getPassword());
		user.setEmail(userDto.getEmail());
		user.setPhone(userDto.getPhone());
		return user;
	}

	/**
	 * Map UserDto from User entity.
	 * @param userDto
	 * @return user
	 */
	@Override
	public UserDto getDTOFromModel(User user) {
		if (user == null) {
			return null;
		}
		UserDto userDto = new UserDto();
		userDto.setId(user.getId());
		userDto.setUsername(user.getUsername());
		userDto.setFirstName(user.getFirstName());
		userDto.setLastName(user.getLastName());
		userDto.setPassword(user.getPassword());
		userDto.setEmail(user.getEmail());
		userDto.setPhone(user.getPhone());
		return userDto;
	}
}
