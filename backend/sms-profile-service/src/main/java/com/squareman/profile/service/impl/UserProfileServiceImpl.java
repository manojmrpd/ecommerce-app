package com.squareman.profile.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.squareman.profile.dto.UserDto;
import com.squareman.profile.entity.User;
import com.squareman.profile.exceptions.BusinessException;
import com.squareman.profile.exceptions.CommonErrorCodes;
import com.squareman.profile.mapper.UserProfileMapper;
import com.squareman.profile.repository.UserRepository;
import com.squareman.profile.service.UserProfileService;
/**
 * 
 * @author Manoj Vurukonda
 *
 */
@Service
public class UserProfileServiceImpl implements UserProfileService {
	/**
	 * User Repository.
	 */
	@Autowired
	private UserRepository userRepository;
	/**
	 * User Profile Mapper.
	 */
	@Autowired
	private UserProfileMapper userMapper;

	/**
	 * get user details from database.
	 * @return users list
	 */
	@Override
	@org.springframework.transaction.annotation.Transactional(readOnly = true)
	public List<UserDto> getUserDetails() {
		List<UserDto> users;
		List<User> userDetails = userRepository.findAll();
		if(null != userDetails && !userDetails.isEmpty()) {
			users = userMapper.getDTOFromModel(userDetails);
			return users;
		} else {
			throw new BusinessException(CommonErrorCodes.USER_DATA_NOT_EXIST);
		}
	}

	/**
	 * creates new user.
	 * @return user
	 */
	@Override
	public UserDto createUser(UserDto userDto) {
		User user = userMapper.getModelFromDTO(userDto);
		User createdUser = userRepository.save(user);
		return userMapper.getDTOFromModel(createdUser);
	}
}
