package com.squareman.profile.exceptions;

import org.springframework.http.HttpStatus;

import com.squareman.profile.constants.Constants;

public enum CommonErrorCodes implements ErrorCodeIF {
	
	NO_CONTENT("9000", 204, "No Content"),
	MANDATORY_HEADERS_MISSING("9001",400, "Missing mandatory header"),
	MISSING_MANDATORY_FIELD("9002",400, "Missing mandatory field"),
	NO_CORRELATION_ID("9003",400, "CorrelationId is missing in headers"),
	NOT_EXPECTED_FIELD("9004",400, "Not expected field"),
	BAD_REQUEST("9005",400, "Bad Request"),
	NO_CLIENTID("9006",400, "ClienId is missing in headers"),
	RECORD_NOT_FOUND("9007",404, "Record Not found"),
	NOT_FOUND("9008",404, "Not found"),
	CONFLICT("9009",404, "Conflict"),
	RECORD_ALREADY_EXIST("9011",409, "Record already exists"),
	MISSING_HTTP_BODY("9012", 422, "Missing Http Request body"),
	
	INTERNAL_SERVER_ERROR("9990", 500, "Internaly server"),
	INTERNAL_SERVICE_ERROR("9991", 500, "Generic unhandled Exception"),
	SERVICE_UNAVAILABLE("9992", 503, "Service Unavailable"),
	UNKNOWN("9999",404, "Unknown"),
	USER_DATA_NOT_EXIST("1001",404, "User data doest not exist");
	
	private String code;
	private int httpCode;
	private String message;
	
	
	private CommonErrorCodes(String code, String message) {
		this.code = code;
		this.message = message;
	}
	
	
	private CommonErrorCodes(String code, int httpCode, String message) {
		this.code = code;
		this.httpCode = httpCode;
		this.message = message;
	}


	public String getCode() {
		return this.code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public int getHttpCode() {
		return httpCode;
	}
	public void setHttpCode(int httpCode) {
		this.httpCode = httpCode;
	}
	public String getMessage() {
		return this.message;
	}
	public void setMessage(String message) {
		this.message = message;
	}


	@Override
	public HttpStatus getHttpStatus() {
		// TODO Auto-generated method stub
		return HttpStatus.valueOf(this.httpCode);
	}


	@Override
	public Error getError(String... customMessage) {
		Error err = new Error();
		StringBuilder sb = new StringBuilder(Constants.EMPTY_STRING);
		err.setCode(this.getCode());
		if(customMessage!=null && customMessage.length>0) {
			for(String str: customMessage) {
				sb.append(str);
			}
		}
		err.setMessage(this.message+sb.toString());
		return err;
	}
	
	
	

	
}
