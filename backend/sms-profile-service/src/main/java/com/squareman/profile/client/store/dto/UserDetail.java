package com.squareman.profile.client.store.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
/**
 * 
 * @author Manoj Vurukonda
 *
 */
@Getter
@Setter
@Data
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor	
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class UserDetail {
	
	@JsonProperty("id")
	private Integer id;
	@JsonProperty("email")
	private String email;
	@JsonProperty("username")
	private String username;
	@JsonProperty("password")
	private String password;
	@JsonProperty("name")
	private UserName name;
	@JsonProperty("phone")
	private String phone;
	@JsonProperty("__v")
	private Integer version;
	@JsonProperty("address")
	private AddressDetail address;
	

}
