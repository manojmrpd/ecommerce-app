package com.squareman.profile.validator;

import java.util.ArrayList;
import java.util.List;
import org.springframework.util.StringUtils;
import com.squareman.profile.constants.Constants;
import com.squareman.profile.constants.ProfileConstants;
import com.squareman.profile.dto.UserDto;
import com.squareman.profile.exceptions.CommonErrorCodes;
import com.squareman.profile.exceptions.Error;

public class UserProfileRequestValidator {

	public static void validateCreateUserRequest(UserDto userDto) {
		
		List<Error> errors = new ArrayList<>();
		validateCommonUserInputs(userDto, errors);
		
	}

	private static void validateCommonUserInputs(UserDto userDto, List<Error> errors) {
		
		if(userDto == null) {
			errors.add(CommonErrorCodes.MISSING_HTTP_BODY.getError());
		} else {
			if(StringUtils.isEmpty(userDto.getId())) {
				errors.add(CommonErrorCodes.MISSING_MANDATORY_FIELD.getError(formatErrorMessage(ProfileConstants.ID)));
			}
			if(StringUtils.isEmpty(userDto.getFirstName())) {
				errors.add(CommonErrorCodes.MISSING_MANDATORY_FIELD.getError(formatErrorMessage(ProfileConstants.FIRSTNAME)));
			}
			if(StringUtils.isEmpty(userDto.getLastName())) {
				errors.add(CommonErrorCodes.MISSING_MANDATORY_FIELD.getError(formatErrorMessage(ProfileConstants.LASTNAME)));
			}
			if(StringUtils.isEmpty(userDto.getUsername())) {
				errors.add(CommonErrorCodes.MISSING_MANDATORY_FIELD
						.getError(formatErrorMessage(ProfileConstants.USERNAME)));
			}
			if(StringUtils.isEmpty(userDto.getEmail())) {
				errors.add(CommonErrorCodes.MISSING_MANDATORY_FIELD
						.getError(formatErrorMessage(ProfileConstants.EMAIL)));
			}
			if(StringUtils.isEmpty(userDto.getPhone())) {
				errors.add(CommonErrorCodes.MISSING_MANDATORY_FIELD
						.getError(formatErrorMessage(ProfileConstants.PHONE)));
			}
		}
		
	}

	private static String formatErrorMessage(String id) {
		return Constants.COLON + Constants.OPEN_BRACKET + id + Constants.CLOSE_BRACKET;
	}
	
	

}
