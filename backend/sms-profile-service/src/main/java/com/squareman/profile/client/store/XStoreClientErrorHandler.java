package com.squareman.profile.client.store;

import java.io.IOException;

import feign.Response;
import feign.codec.ErrorDecoder;
/**
 * 
 * @author Manoj Vurukonda
 *
 */
public class XStoreClientErrorHandler {
	
    /**
     * The default decoder.
     */
    private final ErrorDecoder delegate = new ErrorDecoder() {
		
		@Override
		public Exception decode(String methodKey, Response response) {
			// TODO Auto-generated method stub
			return null;
		}
	};

    /**
     * Raise internal server exception on downstream error.
     * @param methodKey String
     * @param response  Response
     * @return Exception
     */
    public Exception decode(final String methodKey, final Response response) throws IOException {
		if (response.body() != null && response.body().length() > 0) {
			/*
			 * GenericErrorResponse errorResponse =
			 * mapper.readValue(response.body().asInputStream(),
			 * GenericErrorResponse.class); switch (response.status()) { case BAD_REQUEST:
			 * LOGGER.error("Bad Request Exception from feign client"); return new
			 * BadRequestException(errorResponse.getErrorMessage(), ResponseSourceEnum.CPI);
			 * case NOT_FOUND: LOGGER.error("Not Found Exception from feign client"); return
			 * new NotFoundException(errorResponse.getMessage(), ResponseSourceEnum.CPI);
			 * case INTERNAL_SERVER_ERROR:
			 * LOGGER.error("Internal Server Error from feign client"); return new
			 * ServerErrorException(errorResponse.getMessage(), ResponseSourceEnum.CPI);
			 * default: LOGGER.error("Error in request went through feign client"); return
			 * new Exception("Common Feign Exception"); }
			 */}
        return this.delegate.decode(methodKey, response);
    }


}
